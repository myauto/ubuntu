# File /tmp/firstboot.sh will be copy to /target/home/ and set to execute on start by "preseed/late_command" in file "preseed.cfg".

cat > /tmp/firstboot.sh << EOF
#!/bin/bash

# This block is to do some cleaning when the target os's runing.

sed -i "s#bash /home/firstboot.sh##g" /etc/rc.local
rm /home/firstboot.sh

EOF

cat >> /tmp/firstboot.sh << EOF
# This block is to do some basic but important configs when the target os's runing.

apt update

EOF

cat >> /tmp/firstboot.sh << EOF
# This block is to do things that you like when the target os's runing.

sed -i -e "s/PermitRootLogin without-password/PermitRootLogin yes/g" \
-e "s/#PasswordAuthentication yes/PasswordAuthentication yes/g" /etc/ssh/sshd_config

EOF

cat >> /tmp/firstboot.sh << EOF
# reboot to make sure all setings active and not lost.

shutdown -r now

EOF
